//
//  Whistle.swift
//  Project33
//
//  Created by Danni Brito on 4/16/20.
//  Copyright © 2020 Danni Brito. All rights reserved.
//

import CloudKit
import UIKit

class Whistle: NSObject {
    var recordID: CKRecord.ID!
    var genre: String!
    var comments: String!
    var audio: URL!
}
